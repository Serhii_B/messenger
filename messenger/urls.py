from django.conf.urls import include, url
from rest_framework import routers

from .views import UserViewset, MessageViewset

from django.urls import path

api_router = routers.SimpleRouter()
api_router.register(r'user', UserViewset, basename='user')
api_router.register(r'message', MessageViewset, basename='message')

urlpatterns = api_router.urls

# urlpatterns = [
#     # url(r'', include(api_router.urls)),
#     path('users', UserViewset.list, name='read_user'),
#     path('messages/<int:page>', MessageViewset.list_with_pagination, name='message_list_with_pagination'),
# ]
