from django.db import models
from django.core.exceptions import ValidationError
import re


def validate_length(value):
    if len(value) >= 100:
        raise ValidationError('Message text must be less than 100 characters')


def validate_email(value):
    result = re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", value)

    if not result:
        raise ValidationError('Invalid email format')


class User(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=254)
    # email = models.EmailField(max_length=254, validators=[validate_email])


class Message(models.Model):
    text = models.CharField(max_length=99)
    # text = models.CharField(max_length=99, validators=[validate_length])
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
