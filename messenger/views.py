from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from .models import Message, User
from .serializer import MessageSerializer, UserSerializer


class BaseViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return self.model.objects.all()


class MyResultsSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 10


class UserViewset(BaseViewSet):
    serializer_class = UserSerializer
    model = User


class MessageViewset(BaseViewSet):
    qyeryset = Message.objects.all()
    serializer_class = MessageSerializer
    model = Message

    pagination_class = MyResultsSetPagination
